# Cartographile
A D&D mapping tool for you and your players.


## Developing Locally
1. Make the `docker-compose.yml` and `application.yml` files by copying the respective template files.
2. Fill in the yml files with the appropriate data.
3. Run `npm install` in the web module.
4. Run `npm run build -- --watch` to start building the front end. Note it will automatically update when you make changes.
5. Run `docker-compose up` to start up the database and front end server.
6. Run the spring app to get the backend running.
