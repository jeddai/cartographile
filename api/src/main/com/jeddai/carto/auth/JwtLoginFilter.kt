package com.jeddai.carto.auth

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.ProviderManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtLoginFilter(
    private val jwtUtil: JwtUtil,
    private val authProvider: AuthenticationProvider
) : AbstractAuthenticationProcessingFilter("/auth/login") {

    init {
        authenticationManager = ProviderManager(listOf(authProvider))
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication? {
        val mapper = ObjectMapper()
        mapper.registerModule(KotlinModule())
        val credentials = mapper.readValue(request.inputStream, LoginRequestBody::class.java)

        return authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                credentials.username,
                credentials.password,
                emptyList()
            )
        )
    }

    @Throws(IOException::class, ServletException::class)
    override fun successfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain?,
        auth: Authentication
    ) {
        val token = jwtUtil.generateToken(auth.name)

        response.addHeader("Authorization", "Bearer $token")
        response.addHeader("Content-Type", "application/json")
        response.writer.println(true)
    }

    class LoginRequestBody(
        val username: String,
        val password: String
    )
}
