package com.jeddai.carto.auth

import com.jeddai.carto.entity.Player
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.stereotype.Component

@Component
class JwtAuthenticationProvider(
    private val jwtUtil: JwtUtil,
    private val mongoOperations: MongoOperations
) : AuthenticationProvider {

    override fun supports(authentication: Class<*>): Boolean {
        return UsernamePasswordAuthenticationToken::class.java.isAssignableFrom(authentication)
    }

    override fun authenticate(authentication: Authentication): Authentication? {
        return if (authentication is UsernamePasswordAuthenticationToken) {
            val query = Query()
            query.addCriteria(Criteria.where("username").`is`(authentication.principal))

            val player = mongoOperations.findOne(query, Player::class.java)

            if (player != null && player.password == BCrypt.hashpw(authentication.credentials.toString(), player.salt)) {
                UsernamePasswordAuthenticationToken(player.id, player.password)
            } else {
                null
            }
        } else {
            null
        }
    }
}
