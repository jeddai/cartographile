package com.jeddai.carto.auth

import org.jose4j.jwa.AlgorithmConstraints
import org.jose4j.jws.AlgorithmIdentifiers
import org.jose4j.jws.JsonWebSignature
import org.jose4j.jwt.JwtClaims
import org.jose4j.jwt.consumer.ErrorCodes
import org.jose4j.jwt.consumer.InvalidJwtException
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.keys.HmacKey
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.stereotype.Component

@Component
class JwtUtil(
    private val mongoOperations: MongoOperations
) {
    private val secret: String = "yoloswagginsandthefellowshipofthebling"

    fun generateToken(id: String): String {
        val claims = JwtClaims()
        claims.issuer = "cartographile"
        claims.setAudience("Cartographile Player")
        claims.setExpirationTimeMinutesInTheFuture(2880f)
        claims.setGeneratedJwtId()
        claims.setIssuedAtToNow()
        claims.setNotBeforeMinutesInThePast(2f)
        claims.subject = id

        val jws = JsonWebSignature()
        jws.payload = claims.toJson()

        jws.key = HmacKey(secret.toByteArray(Charsets.UTF_8))
        jws.algorithmHeaderValue = AlgorithmIdentifiers.HMAC_SHA256

        return jws.compactSerialization
    }

    fun parseToken(token: String?): JwtClaims? {
        if (token == null) return null

        val jwtConsumer = JwtConsumerBuilder()
            .setRequireExpirationTime()
            .setAllowedClockSkewInSeconds(30)
            .setRequireSubject()
            .setExpectedIssuer("cartographile")
            .setExpectedAudience("Cartographile Player")
            .setVerificationKey(HmacKey(secret.toByteArray(Charsets.UTF_8)))
            .setJwsAlgorithmConstraints(
                AlgorithmConstraints(
                    AlgorithmConstraints.ConstraintType.WHITELIST,
                    AlgorithmIdentifiers.HMAC_SHA256
                )
            )
            .build()

        return try {
            jwtConsumer.processToClaims(token)
        } catch (e: InvalidJwtException) {
            println("Invalid JWT! $e")
            if (e.hasExpired()) {
                println("JWT expired at " + e.jwtContext.jwtClaims.expirationTime)
            }

            if (e.hasErrorCode(ErrorCodes.AUDIENCE_INVALID)) {
                println("JWT had wrong audience: " + e.jwtContext.jwtClaims.audience)
            }

            null
        }
    }
}
