package com.jeddai.carto.auth

import org.springframework.security.authentication.AbstractAuthenticationToken

class JwtAuthenticationToken(
    val token: String
) : AbstractAuthenticationToken(
    null
) {
    override fun getCredentials(): Any {
        return token
    }

    override fun getPrincipal(): String {
        return token
    }
}
