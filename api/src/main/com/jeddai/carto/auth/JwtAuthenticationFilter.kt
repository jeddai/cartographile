package com.jeddai.carto.auth

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class JwtAuthenticationFilter(
    private val jwtUtil: JwtUtil
) : GenericFilterBean() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(
        request: ServletRequest,
        response: ServletResponse,
        filterChain: FilterChain
    ) {
        val token = findToken(request as HttpServletRequest)
        val claims = jwtUtil.parseToken(token)

        SecurityContextHolder.getContext().authentication = if (claims == null) {
            null
        } else {
            UsernamePasswordAuthenticationToken(
                claims.subject,
                null,
                emptyList()
            )
        }
        filterChain.doFilter(request, response)
    }

    private fun findToken(request: HttpServletRequest): String? {
        val header = request.getHeader("Authorization") ?: return null
        return header.replace("Bearer ", "")
    }
}
