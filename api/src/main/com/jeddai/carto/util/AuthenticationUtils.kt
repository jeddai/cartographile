package com.jeddai.carto.util

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class AuthenticationUtils {

    fun loggedInUser(): String {
        return SecurityContextHolder.getContext().authentication.name
    }

    fun loggedInUserCanAccess(matchingId: String): Boolean {
        return SecurityContextHolder.getContext().authentication.name == matchingId
    }
}
