package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "encounters")
class Encounter(
    var campaignId: String,
    var name: String,
    var description: String?
) {

    @Id
    var id: String = ""

    @Transient
    var maps: List<Map> = ArrayList()
}
