package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "")
class Token(
    var image: String,
    var visible: Boolean,
    var xPos: Number,
    var yPos: Number,
    var width: Number,
    var height: Number
) {

    @Id
    var id: String = ""
}
