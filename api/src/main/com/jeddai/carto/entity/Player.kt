package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "players")
data class Player(
    var name: String,
    @Indexed(unique=true) var username: String,
    var password: String,
    var salt: String,
    var bio: String? = null,
    var roles: List<String> = listOf()
) {

    @Id
    var id: String = ""

    @Transient
    var characters: List<Character> = ArrayList()

    @Transient
    var campaigns: List<Campaign> = ArrayList()
}
