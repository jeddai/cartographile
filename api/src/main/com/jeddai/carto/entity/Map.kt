package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "maps")
class Map(
    var encounterId: String,
    var name: String,
    var description: String?
) {

    @Id
    var id: String = ""

    @Transient
    var tokens: List<Token> = ArrayList()
}
