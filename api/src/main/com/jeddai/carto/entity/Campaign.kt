package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "campaigns")
data class Campaign(
    var ownerId: String,
    var name: String,
    var description: String?
) {

    @Id
    var id: String = ""

    @Transient
    var characters: List<Character> = ArrayList()

    @Transient
    var encounters: List<Encounter> = ArrayList()
}
