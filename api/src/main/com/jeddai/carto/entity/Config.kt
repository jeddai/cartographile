package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.ZonedDateTime

@Document(collection = "configuration")
class Config(
    val key: String,
    val value: String,
    val dateAdded: ZonedDateTime
) {
    @Id
    val id: String = ""
}
