package com.jeddai.carto.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "characters")
data class Character(
    var playerId: String,
    var campaignIds: List<String>,
    var name: String,
    var race: String?,
    var characterClasses: List<CharacterClass>?,
    var size: Size?,
    var image: String?
) {
    @Id
    var id: String = ""
}

data class CharacterClass(
    var name: String,
    var level: Number
)

enum class Size {
    TINY,
    SMALL,
    MEDIUM,
    LARGE,
    HUGE,
    GARGANTUAN
}
