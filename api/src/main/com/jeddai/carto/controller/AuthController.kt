package com.jeddai.carto.controller

import com.jeddai.carto.entity.Player
import com.jeddai.carto.repository.PlayerRepository
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/auth")
class AuthController(
    private val playerRepository: PlayerRepository
) {

    @RequestMapping(
        value = ["/create-user"],
        method = [RequestMethod.POST],
        consumes = ["application/json"]
    )
    @ResponseBody
    fun createUser(@RequestBody createUserRequestBody: CreateUserRequestBody): Boolean {
        val salt = BCrypt.gensalt(5)
        val encryptedPassword = BCrypt.hashpw(createUserRequestBody.password, salt)

        val player = Player(
            name = createUserRequestBody.name,
            username = createUserRequestBody.username,
            password = encryptedPassword,
            salt = salt
        )
        player.id = UUID.randomUUID().toString()

        playerRepository.save(player)

        return true
    }

    data class CreateUserRequestBody(
        val name: String,
        val username: String,
        val password: String
    )
}
