package com.jeddai.carto.controller

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.ZonedDateTime

@RestController
class BaseController {

    @RequestMapping(value = ["/xhealth"], produces = ["application/json"])
    fun healthCheck(): ZonedDateTime {
        return ZonedDateTime.now()
    }
}
