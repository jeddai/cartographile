package com.jeddai.carto

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class CartoApplication

fun main(args: Array<String>) {
    runApplication<CartoApplication>(*args)
}
