package com.jeddai.carto.repository

import com.jeddai.carto.entity.Character
import java.util.*

interface CharacterRepositoryCustom {

    fun findByIdAndPlayerId(id: String, playerId: String): Optional<Character>

    fun deleteByIdAndPlayerId(id: String, playerId: String): Boolean
}
