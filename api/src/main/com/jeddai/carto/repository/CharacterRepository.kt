package com.jeddai.carto.repository

import com.jeddai.carto.entity.Character
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CharacterRepository : MongoRepository<Character, String>, CharacterRepositoryCustom
