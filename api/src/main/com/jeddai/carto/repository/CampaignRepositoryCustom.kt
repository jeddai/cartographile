package com.jeddai.carto.repository

import com.jeddai.carto.entity.Campaign
import java.util.*

interface CampaignRepositoryCustom {

    fun findByIdAndPlayerId(id: String, playerId: String): Optional<Campaign>

    fun deleteByIdAndPlayerId(id: String, playerId: String): Boolean
}
