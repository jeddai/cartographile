package com.jeddai.carto.repository

import com.jeddai.carto.entity.Character
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import java.util.*

class CharacterRepositoryImpl(
    private val mongoTemplate: MongoTemplate
) : CharacterRepositoryCustom {

    override fun findByIdAndPlayerId(id: String, playerId: String): Optional<Character> {
        val query = Query()
        query.addCriteria(Criteria.where("id").`is`(id))
        query.addCriteria(Criteria.where("playerId").`is`(playerId))
        return Optional.ofNullable(mongoTemplate.findOne(query, Character::class.java))
    }

    override fun deleteByIdAndPlayerId(id: String, playerId: String): Boolean {
        val query = Query()
        query.addCriteria(Criteria.where("id").`is`(id))
        query.addCriteria(Criteria.where("playerId").`is`(playerId))
        return mongoTemplate.remove(query, Character::class.java).deletedCount > 0
    }
}
