package com.jeddai.carto.repository

import com.jeddai.carto.entity.Campaign
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import java.util.*

class CampaignRepositoryImpl(
    private val mongoTemplate: MongoTemplate
) : CampaignRepositoryCustom {

    override fun findByIdAndPlayerId(id: String, playerId: String): Optional<Campaign> {
        val query = Query()
        query.addCriteria(Criteria.where("id").`is`(id))
        query.addCriteria(Criteria.where("ownerId").`is`(playerId))
        return Optional.ofNullable(mongoTemplate.findOne(query, Campaign::class.java))
    }

    override fun deleteByIdAndPlayerId(id: String, playerId: String): Boolean {
        val query = Query()
        query.addCriteria(Criteria.where("id").`is`(id))
        query.addCriteria(Criteria.where("ownerId").`is`(playerId))
        return mongoTemplate.remove(query, Campaign::class.java).deletedCount > 0
    }
}
