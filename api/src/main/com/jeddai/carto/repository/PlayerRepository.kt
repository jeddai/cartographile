package com.jeddai.carto.repository

import com.jeddai.carto.entity.Player
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface PlayerRepository : MongoRepository<Player, String> {
}
