package com.jeddai.carto.repository

import com.jeddai.carto.entity.Token
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface TokenRepository : MongoRepository<Token, String>
