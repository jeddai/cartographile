package com.jeddai.carto.repository

import com.jeddai.carto.entity.Encounter
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface EncounterRepository : MongoRepository<Encounter, String>
