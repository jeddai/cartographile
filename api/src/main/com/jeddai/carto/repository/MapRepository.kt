package com.jeddai.carto.repository

import com.jeddai.carto.entity.Map
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface MapRepository : MongoRepository<Map, String>
