package com.jeddai.carto.repository

import com.jeddai.carto.entity.Campaign
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface CampaignRepository : MongoRepository<Campaign, String>, CampaignRepositoryCustom
