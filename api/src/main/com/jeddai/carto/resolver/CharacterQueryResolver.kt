package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.jeddai.carto.entity.Character
import com.jeddai.carto.repository.CharacterRepository
import com.jeddai.carto.util.AuthenticationUtils
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class CharacterQueryResolver(
    val characterRepository: CharacterRepository,
    private val mongoOperations: MongoOperations,
    private val authenticationUtils: AuthenticationUtils
) : GraphQLQueryResolver {

    fun characters(): List<Character> {
        val userId = authenticationUtils.loggedInUser()
        val query = Query()
        query.addCriteria(Criteria.where("playerId").`is`(userId))

        return mongoOperations.find(query, Character::class.java)
    }

    fun playerCharacters(playerId: String): List<Character> {
        val query = Query()
        query.addCriteria(Criteria.where("playerId").`is`(playerId))

        return mongoOperations.find(query, Character::class.java)
    }

    fun campaignCharacters(campaignId: String): List<Character> {
        val query = Query()
        query.addCriteria(Criteria.where("campaignId").`is`(campaignId))

        return mongoOperations.find(query, Character::class.java)
    }
}
