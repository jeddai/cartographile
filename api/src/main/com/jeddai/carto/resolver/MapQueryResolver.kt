package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.jeddai.carto.entity.Map
import org.springframework.stereotype.Component

@Component
class MapQueryResolver() : GraphQLQueryResolver {

    fun maps(): List<Map> {
        return listOf()
    }

    fun encounterMaps(encounterId: String): List<Map> {
        return listOf()
    }
}
