package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.jeddai.carto.entity.Campaign
import com.jeddai.carto.entity.Character
import com.jeddai.carto.entity.Player
import com.jeddai.carto.repository.PlayerRepository
import com.jeddai.carto.util.AuthenticationUtils
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class PlayerQueryResolver(
    val playerRepository: PlayerRepository,
    private val mongoOperations: MongoOperations,
    private val authenticationUtils: AuthenticationUtils
) : GraphQLQueryResolver {

    fun players(): List<Player> {
        val userId = authenticationUtils.loggedInUser()

        val query = Query()
        query.addCriteria(Criteria.where("id").`is`(userId))

        val list = mongoOperations.find(query, Player::class.java)
        list.forEach {
            it.characters = getCharacters(playerId = it.id)
            it.campaigns = getCampaigns(playerId = it.id)
        }
        return list
    }

    private fun getCharacters(playerId: String): List<Character> {
        val query = Query()
        query.addCriteria(Criteria.where("playerId").`is`(playerId))
        return mongoOperations.find(query, Character::class.java)
    }

    private fun getCampaigns(playerId: String): List<Campaign> {
        val query = Query()
        query.addCriteria(Criteria.where("ownerId").`is`(playerId))
        return mongoOperations.find(query, Campaign::class.java)
    }
}
