package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.jeddai.carto.entity.Campaign
import com.jeddai.carto.repository.CampaignRepository
import com.jeddai.carto.util.AuthenticationUtils
import java.util.*

class CampaignMutationResolver(
    private val campaignRepository: CampaignRepository,
    private val authenticationUtils: AuthenticationUtils
) : GraphQLMutationResolver {

    fun createCampaign(input: CampaignInput): Campaign {
        val userId = authenticationUtils.loggedInUser()
        val campaign = input.toCampaign(
            playerId = userId
        )
        campaign.id = UUID.randomUUID().toString()

        return campaignRepository.save(campaign)
    }

    fun updateCampaign(id: String, input: CampaignInput): Campaign? {
        val userId = authenticationUtils.loggedInUser()

        val campaign = campaignRepository.findByIdAndPlayerId(id, userId)
        campaign.ifPresent {
            if (input.name != null) it.name = input.name
            if (input.description != null) it.description = input.description

            campaignRepository.save(it)
        }

        return campaign.get()
    }

    fun deleteCampaign(id: String): Boolean {
        val userId = authenticationUtils.loggedInUser()

        return campaignRepository.deleteByIdAndPlayerId(id, userId)
    }
}

class CampaignInput(
    val name: String?,
    val description: String?
)

fun CampaignInput.toCampaign(playerId: String): Campaign {
    return Campaign(
        ownerId = playerId,
        name = name!!,
        description = description
    )
}
