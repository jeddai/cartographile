package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.jeddai.carto.entity.Token
import org.springframework.stereotype.Component

@Component
class TokenQueryResolver() : GraphQLQueryResolver {

    fun tokens(): List<Token> {
        return listOf()
    }

    fun mapTokens(mapId: String): List<Token> {
        return listOf()
    }
}
