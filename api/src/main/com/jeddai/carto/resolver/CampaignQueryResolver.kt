package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.jeddai.carto.entity.Campaign
import com.jeddai.carto.entity.Character
import com.jeddai.carto.entity.Encounter
import com.jeddai.carto.repository.CampaignRepository
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class CampaignQueryResolver(
    val campaignRepository: CampaignRepository,
    private val mongoOperations: MongoOperations
) : GraphQLQueryResolver {

    fun campaigns(): List<Campaign> {
        val list = campaignRepository.findAll()

        populateCampaigns(list)

        return list
    }

    fun ownerCampaigns(ownerId: String): List<Campaign> {
        val query = Query()
        query.addCriteria(Criteria.where("ownerId").`is`(ownerId))

        val list = mongoOperations.find(query, Campaign::class.java)

        populateCampaigns(list)

        return list
    }

    private fun populateCampaigns(list: List<Campaign>) {
        list.forEach {
            it.characters = getCharacters(campaignId = it.id)
            it.encounters = getEncounters(campaignId = it.id)
        }
    }

    private fun getCharacters(campaignId: String): List<Character> {
        val query = Query()
        query.addCriteria(Criteria.where("campaignId").`is`(campaignId))

        return mongoOperations.find(query, Character::class.java)
    }

    private fun getEncounters(campaignId: String): List<Encounter> {
        val query = Query()
        query.addCriteria(Criteria.where("campaignId").`is`(campaignId))

        return mongoOperations.find(query, Encounter::class.java)
    }
}
