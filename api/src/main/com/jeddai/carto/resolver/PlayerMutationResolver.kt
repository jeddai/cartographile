package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.jeddai.carto.entity.Player
import com.jeddai.carto.repository.PlayerRepository
import com.jeddai.carto.util.AuthenticationUtils
import org.springframework.stereotype.Component

@Component
class PlayerMutationResolver(
    private val playerRepository: PlayerRepository,
    private val authenticationUtils: AuthenticationUtils
) : GraphQLMutationResolver {

    fun deletePlayer(id: String): Boolean {
        if (!authenticationUtils.loggedInUserCanAccess(id)) {
            return false
        }

        playerRepository.deleteById(id)

        return true
    }

    fun updatePlayer(
        id: String,
        name: String?,
        username: String?,
        bio: String?
    ): Player? {

        if (!authenticationUtils.loggedInUserCanAccess(id)) {
            return null
        }

        val player = playerRepository.findById(id)
        player.ifPresent {
            if (name != null) it.name = name
            if (username != null) it.username = username
            if (bio != null) it.bio = bio

            playerRepository.save(it)
        }

        return player.get()
    }
}
