package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.jeddai.carto.entity.Encounter
import org.springframework.stereotype.Component

@Component
class EncounterQueryResolver() : GraphQLQueryResolver {

    fun encounters(): List<Encounter> {
        return listOf()
    }

    fun campaignEncounters(campaignId: String): List<Encounter> {
        return listOf()
    }
}
