package com.jeddai.carto.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.jeddai.carto.entity.Character
import com.jeddai.carto.entity.CharacterClass
import com.jeddai.carto.entity.Size
import com.jeddai.carto.repository.CharacterRepository
import com.jeddai.carto.util.AuthenticationUtils
import org.springframework.stereotype.Component
import java.util.*

@Component
class CharacterMutationResolver(
    private val characterRepository: CharacterRepository,
    private val authenticationUtils: AuthenticationUtils
) : GraphQLMutationResolver {

    fun createCharacter(input: CharacterInput): Character {
        val userId = authenticationUtils.loggedInUser()
        val character = input.toCharacter(
            playerId = userId
        )
        character.id = UUID.randomUUID().toString()

        return characterRepository.save(character)
    }

    fun updateCharacter(id: String, input: CharacterInput): Character? {
        val userId = authenticationUtils.loggedInUser()

        val character = characterRepository.findByIdAndPlayerId(id, userId)
        character.ifPresent {
            if (input.name != null) it.name = input.name
            if (input.race != null) it.race = input.race
            if (input.characterClasses != null) it.characterClasses = input.characterClasses.map { characterClassInput ->
                characterClassInput.toCharacterClass()
            }
            if (input.size != null) it.size = input.size
            if (input.image != null) it.image = input.image

            characterRepository.save(it)
        }

        return character.get()
    }

    fun deleteCharacter(id: String): Boolean {
        val userId = authenticationUtils.loggedInUser()

        return characterRepository.deleteByIdAndPlayerId(id, userId)
    }
}

data class CharacterInput(
    val name: String?,
    val race: String?,
    val characterClasses: List<CharacterClassInput>?,
    val size: Size?,
    val image: String?
)

fun CharacterInput.toCharacter(playerId: String): Character {
    return Character(
        playerId = playerId,
        campaignIds = listOf(),
        name = this.name!!,
        race = this.race,
        characterClasses = this.characterClasses?.map {
            it.toCharacterClass()
        },
        size = this.size,
        image = this.image
    )
}

class CharacterClassInput(
    val name: String,
    val level: Number
)

fun CharacterClassInput.toCharacterClass(): CharacterClass {
    return CharacterClass(
        name = name,
        level = level
    )
}
