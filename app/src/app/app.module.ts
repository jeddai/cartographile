import { BrowserModule } from '@angular/platform-browser';
import { forwardRef, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql/graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginService } from './services/login.service';
import { ResourceService } from './services/resource.service';
import { MaterialModule } from './material/material.module';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { TokenPickerComponent } from './components/token-picker/token-picker.component';
import { TokenPickerDialogComponent } from './components/token-picker-dialog/token-picker-dialog.component';
import { MapComponent } from './pages/map/map.component';
import { MapRenderService } from './services/map-render.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CharacterDialogComponent } from './components/character-dialog/character-dialog.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    TokenPickerComponent,
    TokenPickerDialogComponent,
    MapComponent,
    CharacterDialogComponent,
    ConfirmDialogComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    ConfirmDialogComponent,
    CharacterDialogComponent
  ],
  providers: [
    LoginService,
    ResourceService,
    AuthGuard,
    MapRenderService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
