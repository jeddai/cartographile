import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login.service';
import { ResourceService } from './services/resource.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {

  constructor(public loginService: LoginService, public resourceService: ResourceService) {
  }

  ngOnInit(): void {
  }
}
