import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly TOKEN_KEY = 'token';

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  login(username: string, password: string): Observable<HttpResponse<boolean>> {
    return this.httpClient.post<boolean>(`/api/auth/login`, {
      username,
      password
    }, {
      observe: 'response'
    });
  }

  logout() {
    localStorage.removeItem(this.TOKEN_KEY);
    this.router.navigate(['']);
  }

  storeToken(token: string): void {
    localStorage.setItem(this.TOKEN_KEY, token);
  }

  get loggedIn(): boolean {
    return !!localStorage.getItem(this.TOKEN_KEY);
  }
}
