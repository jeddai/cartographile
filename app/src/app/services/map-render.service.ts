import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { faTrash } from '@fortawesome/pro-regular-svg-icons';

@Injectable({
  providedIn: 'root'
})
export class MapRenderService {

  constructor() {
  }

  setupMap(selection, mapData) {
    const MAP_HEIGHT = mapData.height;
    const MAP_WIDTH = mapData.width;

    selection.append('rect')
      .style('fill', 'grey')
      .attr('width', '100%')
      .attr('height', '100%');

    const g = selection.append('g').attr('class', 'map-wrapper');

    g.append('rect')
      .style('fill', 'white')
      .attr('id', 'map')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', MAP_WIDTH)
      .attr('height', MAP_HEIGHT);

    const zoom = d3.zoom()
      .scaleExtent([ 1 / 4, 4 ])
      // .translateExtent([
      //   [MIN_TRANSLATE_X, MIN_TRANSLATE_Y],
      //   [MAX_TRANSLATE_X, MAX_TRANSLATE_Y]
      // ])
      .constrain((transform, extent, translateExtent) => {
        const cx = transform.invertX((extent[1][0] - extent[0][0]) / 2),
          cy = transform.invertY((extent[1][1] - extent[0][1]) / 2),
          dcx0 = Math.min(0, cx - translateExtent[0][0]),
          dcx1 = Math.max(0, cx - translateExtent[1][0]),
          dcy0 = Math.min(0, cy - translateExtent[0][1]),
          dcy1 = Math.max(0, cy - translateExtent[1][1]);
        return transform.translate(
          Math.min(0, dcx0) || Math.max(0, dcx1),
          Math.min(0, dcy0) || Math.max(0, dcy1)
        );
      })
      .on('zoom', () => {
        g.attr('transform', d3.event.transform);
      });

    selection.call(zoom);

    return g;
  }

  addTokens(selection, tokens, mapData) {
    const drag = d3.drag()
      .on('start', function() {
        const obj = d3.select(this).classed('dragging', true);
        let xEnd: number, yEnd: number;

        const dragged = d => {
          const round = (position, resolution) => {
              return position % resolution < resolution / 2 ?
                position - (position % resolution) :
                position + resolution - (position % resolution);
            };
          const x = d3.event.x;
          const y = d3.event.y;
          xEnd = d.snap ? round(Math.max(d.width, Math.min(mapData.width - d.width, x)), mapData.resolution) : x;
          yEnd = d.snap ? round(Math.max(d.height, Math.min(mapData.height - d.height, y)), mapData.resolution) : y;

          obj
            .attr('x', d.x = xEnd)
            .attr('y', d.y = yEnd);
        };

        const ended = () => {
          obj.classed('dragging', false);
          obj.each((d: any /* This is a token object and can be saved */) => {
            console.log('save me here');
          });
        };

        d3.event
          .on('drag', dragged)
          .on('end', ended);
      });

    const handleClick = d => {
      console.log(d);
      selection.selectAll('g.tokens g.options').remove();
      tokensSelection.append('g')
        .attr('id', `${d.id}-options`)
        .attr('class', 'options');
      const optionsSelection = tokensSelection.select('g.options');
      optionsSelection.append('i')
        .attr('class', 'far fa-trash')
        .attr('x', d.x)
        .attr('y', d.y);
    };

    selection.selectAll('g.tokens').remove();
    const tokensSelection = selection.append('g').attr('class', 'tokens');
    tokensSelection.selectAll('image.token')
      .data(tokens)
      .enter()
      .append('image')
      .attr('id', d => d.id)
      .attr('class', d => `token ${d.locked ? 'locked' : ''}`)
      .attr('xlink:href', d => d.path)
      .attr('x', d => d.x)
      .attr('y', d => d.y)
      .attr('height', d => d.height)
      .attr('width', d => d.width)
      .attr('z-index', d => d.z || 50);

    selection.selectAll('image.token')
      .filter(function() {
        return ! this.classList.contains('locked');
      })
      .on('click', handleClick)
      .call(drag);
  }

  addGrid(selection, mapData) {
    const { height, width, resolution } = mapData;
    const data = [];
    for (let i = 0; i <= height; i += resolution) {
      data.push({
        x1: 0,
        y1: i,
        x2: width,
        y2: i
      });
    }

    for (let i = 0; i <= width; i += resolution) {
      data.push({
        x1: i,
        y1: 0,
        x2: i,
        y2: height
      });
    }

    selection.selectAll('g.grid').remove();
    const gridSelection = selection.append('g')
      .attr('class', 'grid');
    gridSelection.selectAll('line')
      .data(data)
      .enter()
      .append('line')
      .attr('x1', d => d.x1)
      .attr('y1', d => d.y1)
      .attr('x2', d => d.x2)
      .attr('y2', d => d.y2)
      .attr('z-index', '100')
      .style('stroke', 'rgba(255, 255, 255, 0.2)')
      .style('stroke-width', 1.5);
  }

  removeGrid(selection) {
    selection.selectAll('line.grid').remove();
  }
}
