import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  fullPath = new BehaviorSubject<string>('/resource/');
  currentPath = new BehaviorSubject<string>('/');
  resources = new BehaviorSubject([]);

  constructor(private httpClient: HttpClient) {
    this.currentPath.subscribe(path => {
      this.indexPath(path);
    });
  }

  indexPath(path: string): void {
    this.httpClient.get<Path[]>(`/resource${path}`)
      .subscribe(response => {
        this.resources.next(response);
        this.fullPath.next(`/resource${path}`);
      });
  }

  indexOut(): void {
    if (! this.canGoBack) return;
    const pathOut = this.currentPath.getValue().slice(1, -1).split('/').slice(0, -1).join('/');
    this.currentPath.next(`/${pathOut}${pathOut === '' ? '' : '/'}`);
  }

  indexIn(path: Path): void {
    if (path.type !== 'directory') return;
    const pathIn = this.currentPath.getValue() + `${path.name}${path.name === '' || path.name.substr(-1) === '/' ? '' : '/'}`;
    this.currentPath.next(pathIn);
  }

  get canGoBack(): boolean {
    return (this.currentPath.getValue().match(/\//g) || []).length > 1;
  }
}

interface Path {
  name: string;
  type: 'file'|'directory';
  mtime: string;
  size?: number;
}
