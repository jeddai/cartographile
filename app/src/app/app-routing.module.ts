import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { MapComponent } from './pages/map/map.component';


const routes: Routes = [{
  path: '',
  component: HomeComponent,
  pathMatch: 'full'
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: 'dashboard',
  component: DashboardComponent,
  canActivate: [ AuthGuard ]
}, {
  path: 'map/:id',
  component: MapComponent,
  canActivate: [ AuthGuard ]
}];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
