import gql from 'graphql-tag';

export const GetCharacters = gql`
  query {
      characters {
          id
          name
          characterClasses {
              name
              level
          }
          race
          size
          image
      }
  }
`;

export const CreateCharacter = gql`
    mutation createCharacter($input: CharacterInput) {
        createCharacter(input: $input) {
            id
            name
            characterClasses {
                name
                level
            }
            race
            size
            image
        }
    }
`;

export const UpdateCharacter = gql`
    mutation updateCharacter($id: ID!, $input: CharacterInput) {
        updateCharacter(id: $id, input: $input) {
            id
            name
            characterClasses {
                name
                level
            }
            race
            size
            image
        }
    }
`;

export const DeleteCharacter = gql`
    mutation deleteCharacter($id: ID!) {
        deleteCharacter(id: $id)
    }
`;
