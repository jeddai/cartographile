import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-token-picker-dialog',
  templateUrl: './token-picker-dialog.component.html',
  styleUrls: ['./token-picker-dialog.component.scss']
})
export class TokenPickerDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TokenPickerDialogComponent>) { }

  ngOnInit() {

  }

}
