import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenPickerDialogComponent } from './token-picker-dialog.component';

describe('TokenPickerDialogComponent', () => {
  let component: TokenPickerDialogComponent;
  let fixture: ComponentFixture<TokenPickerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenPickerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenPickerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
