import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenPickerComponent } from './token-picker.component';

describe('TokenPickerComponent', () => {
  let component: TokenPickerComponent;
  let fixture: ComponentFixture<TokenPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
