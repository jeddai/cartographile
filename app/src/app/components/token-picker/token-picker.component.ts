import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ResourceService } from '../../services/resource.service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-token-picker',
  templateUrl: './token-picker.component.html',
  styleUrls: ['./token-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => TokenPickerComponent),
    }
  ]
})
export class TokenPickerComponent implements OnInit, ControlValueAccessor {

  @Input() itemHeight = '150px';
  @Input() listWidth = '450px';
  @Input() columns = 3;
  $token = new BehaviorSubject('');
  onChange = (token: string) => {};
  onTouch = () => {};

  constructor(public resourceService: ResourceService) { }

  ngOnInit() {
  }

  select(resource: any): void {
    if (resource.type === 'directory') {
      this.resourceService.indexIn(resource);
    } else {
      this.token = this.resourceService.fullPath.getValue() + resource.name;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(token: string): void {
    this.token = token;
  }

  get token(): string {
    return this.$token.getValue();
  }

  set token(token: string) {
    this.$token.next(token);
    this.onChange(token);
    this.onTouch();
  }
}
