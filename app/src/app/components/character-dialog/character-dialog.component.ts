import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { faMinus, faPlus } from '@fortawesome/pro-regular-svg-icons';

@Component({
  selector: 'app-create-character-dialog',
  templateUrl: './character-dialog.component.html',
  styleUrls: [ './character-dialog.component.scss' ]
})
export class CharacterDialogComponent implements OnInit {

  createCharacterForm: FormGroup;
  faMinus = faMinus;
  faPlus = faPlus;

  constructor(private dialogRef: MatDialogRef<CharacterDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    const character = this.data.character || {};

    this.createCharacterForm = new FormGroup({
      id: new FormControl(character.id || null),
      name: new FormControl(character.name || '', [Validators.required, Validators.minLength(1)]),
      race: new FormControl(character.race || ''),
      characterClasses: new FormArray([]),
      size: new FormControl(character.size || null),
      image: new FormControl(character.image || '')
    });

    if (character.characterClasses) {
      character.characterClasses.forEach(characterClass => {
        const array = this.createCharacterForm.controls.characterClasses as FormArray;
        array.push(new FormGroup({
          name: new FormControl(characterClass.name || ''),
          level: new FormControl(characterClass.level || 0)
        }));
      });
    }
  }

  addCharacterClass() {
    const array = this.createCharacterForm.controls.characterClasses as FormArray;
    array.controls.push(new FormGroup({
      name: new FormControl(''),
      level: new FormControl(0)
    }));
  }

  getFormArray(name: string): FormArray {
    return this.createCharacterForm.get(name) as FormArray;
  }

  removeCharacterClass(i: number) {
    const array = this.createCharacterForm.controls.characterClasses as FormArray;
    array.removeAt(i);
  }

  close() {
    this.dialogRef.close();
  }

}
