import { Component, OnDestroy, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { CreateCharacter, DeleteCharacter, GetCharacters, UpdateCharacter } from '../../graphql/Characters';
import { BehaviorSubject } from 'rxjs';
import { faPlus } from '@fortawesome/pro-regular-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { CharacterDialogComponent } from '../../components/character-dialog/character-dialog.component';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss' ]
})
export class DashboardComponent implements OnInit, OnDestroy {

  $userCharacters = new BehaviorSubject([]);
  $userCampaigns = new BehaviorSubject([]);
  loading = false;
  querySubscription;
  faPlus = faPlus;

  constructor(public apollo: Apollo, private dialogRef: MatDialog) {
  }

  ngOnInit() {
    this.loading = true;
    this.querySubscription = this.apollo.watchQuery<any>({
      query: GetCharacters
    }).valueChanges.subscribe(({ data, loading }) => {
      this.loading = loading;
      this.$userCharacters.next(data.characters);
    });
  }

  ngOnDestroy() {
    this.querySubscription.unsubscribe();
  }

  createOrSaveCharacter(character?: any) {
    const dialog = this.dialogRef.open(CharacterDialogComponent, {
      width: '75vw',
      data: {
        character: character || {}
      }
    });

    dialog.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        let mutation = CreateCharacter;
        const variables: any = {
          input: result
        };
        if (!!result.id) {
          variables.id = result.id;
          mutation = UpdateCharacter;
        }
        delete result.id;

        this.apollo.mutate({
          mutation,
          variables,
          refetchQueries: [{
            query: GetCharacters
          }]
        }).subscribe(({ data }) => {
          console.log('got data', data);
        }, (error) => {
          console.error('there was an error sending the query', error);
        });
      }
    });
  }

  deleteCharacter(character) {
    const dialog = this.dialogRef.open(ConfirmDialogComponent, {
      width: '500px',
      data: {
        character
      }
    });

    dialog.afterClosed().subscribe(result => {
      if (result) {
        this.apollo.mutate({
          mutation: DeleteCharacter,
          variables: {
            id: result.id
          },
          refetchQueries: [{
            query: GetCharacters
          }]
        }).subscribe(({ data }) => {
          console.log('got data', data);
        }, (error) => {
          console.error('there was an error sending the query', error);
        });
      }
    });
  }

}
