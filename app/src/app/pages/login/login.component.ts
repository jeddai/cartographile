import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl('', [ Validators.required ]),
      password: new FormControl('', [ Validators.required ])
    });
  }

  login(): void {
    const { username, password } = this.loginForm.value;
    this.loginService.login(username, password)
      .subscribe(response => {
        if (response.body === true) {
          const token = response.headers.get('Authorization').substring(7);
          this.loginService.storeToken(token);

          this.router.navigate(['dashboard']);
        }
      });
  }

}
