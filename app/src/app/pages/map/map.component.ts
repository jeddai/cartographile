import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import * as d3 from 'd3';
import { MapRenderService } from '../../services/map-render.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {

  height = '90vh';
  width = '100vw';
  mapData = {
    height: 4000,
    width: 4000,
    resolution: 38.09
  };
  $showGrid = new BehaviorSubject(true);
  $tokens = new Subject<any[]>();
  svg;
  map;
  tokenSubscription;

  constructor(private mapRenderService: MapRenderService) { }

  ngOnInit() {
    this.svg = d3.select('svg');

    this.map = this.mapRenderService.setupMap(this.svg, this.mapData);

    this.tokenSubscription = combineLatest(
      this.$tokens,
      this.$showGrid
    ).subscribe(([tokens, showGrid]) => {
      this.mapRenderService.addTokens(this.map, tokens, this.mapData);

      if (showGrid) {
        this.mapRenderService.addGrid(this.map, this.mapData);
      } else {
        this.mapRenderService.removeGrid(this.map);
      }
    });

    this.$tokens.next([
      {
        path: '/resource/tokens/maps/Temple of Black Earth.png',
        x: 1,
        y: 2.2,
        height: 3500,
        width: 2700,
        locked: true,
        id: 'saoidfjios'
      },
      {
        path: '/resource/tokens/heroes/11.png',
        x: 1028.43,
        y: 457.08000000000004,
        height: this.mapData.resolution,
        width: this.mapData.resolution,
        z: 55,
        snap: true,
        id: 'oasdjfoijsa'
      },
      {
        path: '/resource/tokens/heroes/15.png',
        x: 1000,
        y: 440,
        height: this.mapData.resolution,
        width: this.mapData.resolution,
        z: 55,
        snap: false,
        id: 'oasdjfoijsa'
      }
    ]);

  }

  ngOnDestroy() {
    this.tokenSubscription.unsubscribe();
  }

}
